import React from "react";
import { selectSong } from "./actions";
import { connect } from "react-redux";

class SongList extends React.Component {

	constructor(props) {
		super(props);

		this.state = {
			algo2: 'oi'
		}
	}
	
	_handleClick = (song) => {
		this.props.algo1(song);
	}

	render() {

		console.log(this.state.algo2);

		const list = this.props.songs.map((song) => {
			return (
				<div> 
					{song.name} 
					<button onClick={this._handleClick.bind(null, song.name)}> 
						Selecionar 
					</button> 
				</div>
			);
		});

		return (
			<div>
				<button onClick={() => this.setState({algo2: 'tchau'})}>Teste</button>
				{list}
			</div>
		);
	}

}

const mapDispatchToProps = (dispatch) => {
	return {
		algo1: (song) => dispatch(selectSong(song)) 
	}
}

export default connect(null, mapDispatchToProps)(SongList);