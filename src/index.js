import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import { createStore } from "redux";
import reducers from "./reducers";
import { Provider } from "react-redux";

export const store = createStore(reducers);

const Application = () => {
	return (
		<Provider store={store}>
			<App/>
		</Provider>
	);
}


ReactDOM.render(<Application />, document.getElementById('root'));
