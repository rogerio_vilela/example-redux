import React from 'react';
import SongList from "./SongList";
import SongSelected from "./SongSelected";

class App extends React.Component {
	
	render() {
		const songs = [
			{name: 'musica 1'},
			{name: 'musica 2'},
			{name: 'musica 3'},
			{name: 'musica 4'},
		];

		return (
			<div> 
				<SongList songs={songs}/> 
				<SongSelected/>
			</div>

		);
	}
}

export default App;