import React from "react";
import { connect } from "react-redux";

class SongSelected extends React.Component {
	
	render() {
		return (
			<div> Música selecionada: {this.props.algo} </div>
		);
	}
}

const mapStateToProps = (store) => {
	return {
		algo: store.selectedSong
	}
}

export default connect(mapStateToProps)(SongSelected);