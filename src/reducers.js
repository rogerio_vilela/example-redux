import { combineReducers } from "redux";

const selectedSongReducer = (selectedSong = "musica 5", action) => {

	console.log("Me enviaram uma action, sou o selected");

	if(action.type === "SELECT_SONG") {
		return action.payload
	}

	return selectedSong;
	
}

const xReducer = (x = 1, action) => {

	console.log("Me enviaram uma action, sou o x");

	return x;
	
}

export default combineReducers({
	selectedSong: selectedSongReducer,
	x: xReducer,
})